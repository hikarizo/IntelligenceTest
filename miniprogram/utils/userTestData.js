const db = wx.cloud.database()
const app = getApp()

function getUserData(openId) {
  var data = []
  db.collection("userData").doc(openId).get({
    // 数据库中有该用户信息
    success: function (res) {
      console.log(res.data)
    },
    // 数据库中没有，则添加该用户数据，再返回新创建的值
    fail: function (res) {
      db.collection("userData").add({
        data: {
          data: [],
          _id: openId
        },
        success: function (res) {
          var newData = {
            data: [],
            _id: openId
          }
           console.log(newData)
        }
      })
    }
  })
}

module.exports = {
  getUserData: getUserData
}