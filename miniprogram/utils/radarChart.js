data: {
  var numCount = 5; //元素个数
  var numSlot = 5; //一条线上的总节点数
  var windowW = wx.getSystemInfoSync().windowWidth;
  var mCenter = windowW/2; //中心点
  var mAngle = Math.PI * 2 / numCount; //角度
  var mRadius = mCenter-80; //半径
  var radCtx = "" //图形对象
  var score = "" //得分数据
  var mText = [
    "知觉辨别",
    "类同比较",
    "比较推理",
    "系列关系",
    "抽象推理"
  ]
}

function drawChart(id,data) {
  radCtx = wx.createCanvasContext(id)
  score = data
  this.drawEdge()
  this.drawLinePoint()
  this.drawTextCans()
  this.drawRegion()
  this.drawCircle()
  radCtx.draw()
}

// 第一步：绘制6条边
function drawEdge() {
  radCtx.setStrokeStyle("#CCCCCC")
  radCtx.setLineWidth(2) //设置线宽
  for (var i = 0; i < numSlot; i++) {
    //计算半径
    radCtx.beginPath()
    var rdius = mRadius / numSlot * (i + 1)
    //画6条线段
    for (var j = 0; j < numCount; j++) {
      //坐标
      var x = mCenter + rdius * Math.cos(-(Math.PI / 2 - mAngle * j));
      var y = mCenter + rdius * Math.sin(-(Math.PI / 2 - mAngle * j));
      radCtx.lineTo(x, y);
    }
    radCtx.closePath()
    radCtx.stroke()
  }
}

// 第二步：绘制连接点
function drawLinePoint() {
  radCtx.beginPath();
  for (var k = 0; k < numCount; k++) {
    var x = mCenter + mRadius * Math.cos(-(Math.PI / 2 - mAngle * k));
    var y = mCenter + mRadius * Math.sin(-(Math.PI / 2 - mAngle * k));
    radCtx.moveTo(mCenter, mCenter);
    radCtx.lineTo(x, y);
  }
  radCtx.stroke();
}

//第三步：绘制文字（文字位置可能需要微调）
function drawTextCans() {
  radCtx.setFillStyle("#328eeb")
  radCtx.setFontSize(16) //设置字体
  for (var n = 0; n < numCount; n++) {
    var x = mCenter + mRadius * Math.cos(-(Math.PI / 2 - mAngle * n));
    var y = mCenter + mRadius * Math.sin(-(Math.PI / 2 - mAngle * n));
    // radCtx.fillText(mData[n][0], x, y);
    //通过不同的位置，调整文本的显示位置
    if (n==0) {
      radCtx.fillText(mText[n], x-radCtx.measureText(mText[n]).width/2, y-5);
    } else if (n==1) {
      radCtx.fillText(mText[n], x-10, y-10);
    } else if (n==2||n==3) {
      radCtx.fillText(mText[n], x-radCtx.measureText(mText[n]).width/2, y+17);
    } else {
      radCtx.fillText(mText[n], x-radCtx.measureText(mText[n]).width+10, y-10);
    }
  }
}


//绘制数据区域(数据和填充颜色)
function drawRegion(){
  radCtx.beginPath();
  for (var m = 0; m < numCount; m++){
  var x = mCenter + mRadius * Math.cos(-(Math.PI / 2 - mAngle * m)) * score[m].score / 12;
  var y = mCenter + mRadius * Math.sin(-(Math.PI / 2 - mAngle * m)) * score[m].score / 12;
  radCtx.lineTo(x, y);
  }
  radCtx.closePath();
  radCtx.setFillStyle("rgba(50,142,235,0.2)")
  radCtx.fill();
  }

  
//画点
function drawCircle(){
  var r = 3; //设置节点小圆点的半径
  for(var i = 0; i<numCount; i ++){
  var x = mCenter + mRadius * Math.cos(-(Math.PI / 2 - mAngle * i)) * score[i].score / 12;
  var y = mCenter + mRadius * Math.sin(-(Math.PI / 2 - mAngle * i)) * score[i].score / 12;
  radCtx.beginPath();
  radCtx.arc(x, y, r, 0, Math.PI * 2);
  radCtx.fillStyle= "rgba(50,142,235,0.8)";
  radCtx.fill();
  }
  }

module.exports = {
  drawChart,
  drawEdge,
  drawLinePoint,
  drawTextCans,
  drawRegion,
  drawCircle
}