const app = getApp()
// 获取相关用户信息的函数

//获取用户微信信息（头像、昵称等）
function login(e){
    //获取成功
    if(e.detail.errMsg=="getUserInfo:ok"){
      // 将信息存入全局变量
      app.globalData.userInfo=e.detail.userInfo
      app.globalData.login_type=1
      console.log("登陆成功")
      return(true)
    } else{
      app.globalData.login_type=0
      console.log("获取授权失败")
      return(false)
    }
}

// 暴露接口
module.exports = {
  login
}