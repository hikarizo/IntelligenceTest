// miniprogram/test/test.js
const db = wx.cloud.database()
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 云存储地址
    cloud_path: "cloud://mytest-1224523355.6d79-mytest-1224523355-1302337470/questions/",
    // 与用户年龄匹配的标准分
    standardScore:[],
    // 当前题号及总题数
    no_now: 1,
    no_sum: 60,
    // 当前进度
    progress_percent: "",
    // 问题图片地址
    questionSrc: "",
    // 当前题号的选项个数
    answer_count: "6",
    // 当前题号的分类
    question_type: "",
    // 所有选项的数据
    answer: [],
    // 当前选中的选项
    select_answer: -1,
    // 该题答案
    key: "",
    // 得分
    score: [{
      type: "知觉辨别能力",
      score: 0
    }, {
      type: "类同比较能力",
      score: 0
    }, {
      type: "比较推理能力",
      score: 0
    }, {
      type: "系列关系能力",
      score: 0
    }, {
      type: "抽象推理能力",
      score: 0
    }]
  },

  // 进入下一题时初始化数据
  initData: function (e) {
    var progress_percent = (this.data.no_now / this.data.no_sum) * 100
    var answer = []
    var select_answer = -1
    var key = ""
    this.setData({
      progress_percent: progress_percent,
      answer: answer,
      select_answer: select_answer,
      key: key
    })
  },

  // 获取新的题目信息，参数为题号
  getNewQuestion: function (e) {
    var that = this
    var questionId = "question-" + e
    db.collection('questions').doc(questionId).get({
      success: function (res) {
        that.setData({
          answer_count: res.data.count,
          key: res.data.key,
          question_type: res.data.type
        })
        // 获取题目图片
        that.getQuestionImage(e)
        // 获取选项信息
        that.initOptions(that.data.answer_count)
      }
    })
  },

  // 初始化所有答案选项
  initOptions: function (e) {
    let answer = this.data.answer
    for (var i = 1; i <= e; i++) {
      var newarray = {
        id: i,
        isSelected: false,
        // image_src: this.data.cloud_path + this.data.no_now + "/" + i + ".png"
        image_src:"../../images/question/" + this.data.no_now + "/" + i + ".png"
      }
      answer = answer.concat(newarray)
    }
    // 获取初始化选项信息
    this.setData({
      answer: answer
    })
  },

  // 获取题目图片
  getQuestionImage: function (e) {
    // var src = this.data.cloud_path + e + "/question.png"
    var src = "../../images/question/" + e + "/question.png"
    this.setData({
      questionSrc: src
    })
  },

  // 获取当前进度百分比
  getProgress: function () {
    var percent = (this.data.no_now / this.data.no_sum) * 100
    this.setData({
      progress_percent: percent
    })
  },

  // 当单选按钮变动时记录当前选中选项
  radioChange: function (e) {
    let select = parseInt(e.detail.value)
    this.setData({
      select_answer: select
    })
  },

  // 下一题
  nextQuestion: function (e) {
    // 检查选项是否为空
    if (this.data.select_answer != -1) {
      // 判断对错
      if (this.data.select_answer == this.data.key) {
        console.log("回答正确")
        let score = this.data.score
        score[this.data.question_type - 1].score++
        this.setData({
          score: score
        })
      } else {
        console.log("回答错误")
      }
      // 判断当前是否是最后一题
      if (this.data.no_now != this.data.no_sum) {
        // 初始化数据
        this.initData()
        // 题号+1
        var no_now = this.data.no_now + 1
        this.setData({
          no_now: no_now
        })
        // 获取下一题信息
        this.getNewQuestion(this.data.no_now)
        // 更新进度条
        this.getProgress()

      } else {

        // 将得分数据存入全局变量
        app.globalData.score = this.data.score

        // 计算正确的题数
        var score_sum = 0
        for (var i = 0; i < this.data.score.length; i++) {
          score_sum += this.data.score[i].score
        }
        // 计算智力评分等级
        var IQ_level = ""
        for (var i = 0; i < 8; i++) {
          if (i == 0 && score_sum >= this.data.standardScore[0]) {
            IQ_level = 0
            break
          } else if (score_sum >= this.data.standardScore[i]) {
            IQ_level += i
            break
          } else if (i == 7) {
            IQ_level = 7
          }
        }

        // 检查是否已经登陆
        if (app.globalData.login_type == 1) {

          // 上传数据至数据库
          var openId = app.globalData.openId
          // var username = app.globalData.userInfo.nickname
          var username = "祈祷"
          var addData = {
            score: this.data.score,
            date: new Date(),
            age: this.data.age,
            IQ_level: IQ_level
          }
          db.collection("userData").doc(openId).update({
            data: {
              username: username,
              data: db.command.push(addData)
            },
            success: function (res) {
              // 是否是老用户
              if (res.stats.updated != 0) {
                console.log("数据上传成功：")
                console.log(res)
              } else {
                // 若无该用户则创建新的用户条目
                console.log("用户不存在，创建新用户")
                db.collection("userData").add({
                  data: {
                    _id: openId,
                    username: username,
                    data: [addData]
                  },
                  success: function (res) {
                    console.log("创建成功：")
                    console.log(res)
                  }
                })
              }
            },

          })
        }

        // 进入结算界面
        wx.redirectTo({
          url: '../../pages/score/score?' + "previousPage=testPage" + "&IQ_level=" + IQ_level
        })
      }
    } else {
      wx.showToast({
        title: '选项不能为空',
        icon: "none"
      })
    }
  },

  // 计算最终成绩
  getStandardScore: function (age) {
    var that = this
    var id = ""
    // 判断用户以哪个年龄的标准分来评估成绩
    if (age > 0 && age <= 5.5) {
      id = "age-5.5"
    } else if (age > 5.75 && age <= 17) {
      for (var i = 6; i <= 16; i += 0.5) {
        if (age <= i + 0.25) {
          id = "age-" + i
          break
        }
      }
    } else if (age > 17 && age <= 20) {
      id = "age-17"
    } else if (age > 20 && age <= 30) {
      id = "age-20"
    } else if (age > 30 && age <= 40) {
      id = "age-30"
    } else if (age > 40 && age <= 50) {
      id = "age-40"
    } else if (age > 50 && age <= 60) {
      id = "age-50"
    } else if (age > 60 && age <= 70) {
      id = "age-60"
    } else if (age > 70) {
      id = "age-70"
    }
        console.log("ID:"+id)
    db.collection('IQ_level').doc(id).get({
      success: function (res) {
        let sdScore = res.data.standardScore

        // 更新标准分
        that.setData({
          standardScore: sdScore
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 查询该年龄的标准分
    this.getStandardScore(options.age)
    // 从数据库中获取题目信息
    this.getNewQuestion(this.data.no_now)
    // 计算当前进度百分比
    this.getProgress()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})