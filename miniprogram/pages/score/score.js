// miniprogram/pages/score/score.js
const app = getApp()
const db = wx.cloud.database()
var radarChart = require("../../utils/radarChart.js")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    comment_type: [{
      IQ: "125以上",
      text: "智力超常，天才"
    }, {
      IQ: "119～124",
      text: "智力优秀，非常聪明"
    }, {
      IQ: "110～118",
      text: "智力中上，聪明"
    }, {
      IQ: "100～110",
      text: "智力中等，正常"
    }, {
      IQ: "90～100",
      text: "智力一般，稍差"
    }, {
      IQ: "81～89",
      text: "智力中下，反应迟钝"
    }, {
      IQ: "76～80",
      text: "智力低下"
    }, {
      IQ: "75以下",
      text: "智力存在缺陷"
    }],
    comment_text: "",
    IQ: "",


  },

  // 获取评价文本
  getComment: function (IQ_level) {
    console.log("IQ_level:"+IQ_level)
    let comment_text = this.data.comment_type[IQ_level].text
    let IQ = this.data.comment_type[IQ_level].IQ
    this.setData({
      comment_text: comment_text,
      IQ: IQ
    })
  },



  backToHome: function (e) {
    wx.navigateBack({
      complete: (res) => {},
    })
  },

  loadPage: function (score, IQ_level) {
    // 绘制雷达图
    radarChart.drawChart("radarCanvas", score)
    // 获取评价文本
    this.getComment(IQ_level)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var score = ""
    var IQ_level = ""
    var that = this
    // 判定是从哪个页面进入该得分详情页面的
    if (options.previousPage == "testPage") {
      // 接收得分情况
      score = app.globalData.score
      IQ_level = options.IQ_level
      that.loadPage(score, IQ_level)
    } else if (options.previousPage == "historyPage") {
      db.collection("userData").doc(app.globalData.openId).get({
        success: function (res) {
          score = res.data.data[options.index].score
          IQ_level = res.data.data[options.index].IQ_level
          that.loadPage(score, IQ_level)
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})