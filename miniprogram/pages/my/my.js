// miniprogram/my/my.js
const app = getApp()
let userInfo = require("../../utils/userInfo.js")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    avatarUrl: '../../images/user-unlogin.png',
    username:"游客",
    // 登陆按钮的显示状态，ture为显示，false为影藏
    loginButton_isShow: true
  },

  getMyInfo:function(e){
    if(userInfo.login(e)){
      // 更新Data中的值
      this.setData({
      avatarUrl:app.globalData.userInfo.avatarUrl,
      username:app.globalData.userInfo.nickName,
      loginButton_isShow: false
    })
    }
  },

  goToHistory:function(e){
    if(app.globalData.login_type==1){
      wx.navigateTo({
        url: '../../pages/history/history',
      })
    }else{
      wx.showToast({
        title: '请先登陆账户',
        icon:"none"
      })
    }
  },

  goToAbout:function(e){
    wx.navigateTo({
      url: '../../pages/info/info',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
// 获取用户信息
wx.getSetting({
  success: res => {
    // 检测用户是否已经授权
    if (res.authSetting['scope.userInfo']) {
      // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
      wx.getUserInfo({
        success: res => {
          // 将用户信息存入全局变量中
          app.globalData.userInfo = res.userInfo
          // 更新Data中的值
          this.setData({
            avatarUrl: app.globalData.userInfo.avatarUrl,
            username: app.globalData.userInfo.nickName
          })
          // 更改登陆状态
          app.globalData.login_type = 1
          // 隐藏登陆按钮
          this.setData({
            loginButton_isShow: false
          })
        }
      })
    } else {
      // 未授权时以游客身份使用
      app.login_type = 0
      this.setData({
        avatarUrl: "../../images/user-unlogin.png",
        username: "游客"
      })
      // 更改登陆状态
      app.globalData.login_type = 0
      // 显示登陆按钮
      this.setData({
        loginButton_isShow: true
      })
    }
  }
})
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})