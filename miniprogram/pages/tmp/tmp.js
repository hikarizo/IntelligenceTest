// miniprogram/pages/tmp/tmp.js

const db = wx.cloud.database()
const app = getApp()
const util = require("../../utils/util.js")
const userTestData = require("../../utils/userTestData.js")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    score:[{
      name: "知觉辨别能力",
      score: 5
    }, {
      name: "类同比较能力",
      score: 8
    }, {
      name: "比较推理能力",
      score: 7
    }, {
      name: "系列关系能力",
      score: 6
    }, {
      name: "抽象推理能力",
      score: 9
    }]
  },

  inputScore:function(e){
    this.setData({
      scoreStr:e.detail.value
    })
  },

  inputAge:function(e){
    this.setData({
      age:e.detail.value
    })
  },

  add:function(e){
    var date = new Date()
    var score = this.data.score
    var data = []
    data.push({date,score})
    var openId = "oefev4nUqIkjVHMQTgIqlLEwYVAI"
    var userData = {openId,data}

    console.log(userTestData.getUserData(openId))

    // db.collection("userData").where({
    //     openId:"oefev4nUqIkjVHMQTgIqlLEwYVAI"
    // }).get({
    //   success:function(res){
    //     var ddd = res
    //     console.log(res.data[])
    //     baada3ac5edcadf8006c589e47c4b164
    //     oefev4nUqIkjVHMQTgIqlLEwYVAI
    //   }
    // })


  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})