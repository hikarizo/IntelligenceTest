// miniprogram/test/test.js
//引入全局变量
const app = getApp()
const userInfo = require("../../utils/userInfo.js")
const util = require("../../utils/util")


Page({

  /**
   * 页面的初始数据
   */
  data: {
    avatarUrl: "",
    username: "",
    openId: "",
    age: -1,
    // 登陆按钮的显示状态，ture为显示，false为影藏
    loginButton_isShow: true,
    // 当前时间
    nowDate: util.formatDate(new Date()),

    // 出生年月文本显示控制
    isTipShow: true,
    isBrithdayShow: false,
    brithday: ""
  },

  // 获取用户openid
  getOpenId() {
    let that = this;
    wx.cloud.callFunction({
      name: 'getOpenId',
      complete: res => {
        // console.log('云函数获取到的openid: ', res.result.openId)
        var openId = res.result.openId;
        that.setData({
          openId: openId
        })
        // 将OpenId存入全局变量中
        app.globalData.openId = openId
      }
    })
  },

  getMyInfo: function (e) {
    if (userInfo.login(e)) {
      // 更新Data中的值
      this.setData({
        avatarUrl: app.globalData.userInfo.avatarUrl,
        username: app.globalData.userInfo.nickName,
        // 隐藏登陆按钮
        loginButton_isShow: false
      })

    }
  },

  // 开始测试
  startTest: function (e) {
    if (this.data.age != -1) {
      wx.navigateTo({
        url: '../test/test?age=' + this.data.age,
      })
    } else {
      wx.showToast({
        title: '请先填写出生年月',
        icon: "none"
      })
    }
  },

  // 修改出生年月
  birthdayChange: function (e) {
    var brithday = e.detail.value
    var brithday_year = parseInt(brithday.substring(0, 4))
    var brithday_month = parseInt(brithday.substring(5, 7))
    var age = (new Date().getFullYear() - brithday_year) + ((new Date().getMonth() + 1 - brithday_month) / 12)
    this.setData({
      age: age,
      // 修改显示文本
      isTipShow: false,
      isBrithdayShow: true,
      brithday: brithday
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取用户openId
    this.getOpenId()


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 获取用户信息
    wx.getSetting({
      success: res => {
        // 检测用户是否已经授权
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 将用户信息存入全局变量中
              app.globalData.userInfo = res.userInfo
              // 更新Data中的值
              this.setData({
                avatarUrl: app.globalData.userInfo.avatarUrl,
                username: app.globalData.userInfo.nickName
              })
              // 更改登陆状态
              app.globalData.login_type = 1
              // 隐藏登陆按钮
              this.setData({
                loginButton_isShow: false
              })
            }
          })
        }else{
          // 未授权时以游客身份使用
          app.login_type = 0
          this.setData({
            avatarUrl: "../../images/user-unlogin.png",
            username: "游客"
          })
          // 更改登陆状态
          app.globalData.login_type = 0
          // 显示登陆按钮
          this.setData({
            loginButton_isShow: true
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})