// miniprogram/pages/history/history.js

const app = getApp()
const db = wx.cloud.database()
const util = require("../../utils/util.js")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    data: [],
    comment_type: ["天才", "非常聪明", "聪明", "正常", "智力稍差", "反应迟钝", "智力低下", "存在缺陷"],
    dataCount:""
  },

  goToDetailPage: function (e) {
    // 将index由时间倒序改为正序
    let index = e.currentTarget.dataset.index
    wx.navigateTo({
      url: '../../pages/score/score?' + "previousPage=historyPage" + "&index=" + index,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    console.log(app.globalData.openId)
    db.collection("userData").doc(app.globalData.openId).get({
      success: function (res) {
        
        var rData = res.data.data
        var getData = []
        for (var i = rData.length - 1; i >= 0; i--) {
          var comment = that.data.comment_type[rData[i].IQ_level]
          var newData = {
            index: i,
            date: util.formatDate(rData[i].date),
            comment: comment
          }
          getData.push(newData)
        }     
        that.setData({
          data:getData,
          dataCount: getData.length
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})